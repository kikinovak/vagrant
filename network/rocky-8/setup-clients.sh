#!/bin/bash
#
# setup-clients.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# DNS
echo "Configuring DNS resolution."
cp -v /vagrant/resolv.conf /etc

# Gateway
echo "Configuring default gateway."
route del default
route add default gw 10.23.45.10

exit 0
