# Vagrantfile

ENV['VAGRANT_NO_PARALLEL'] = 'yes'
NETWORK_PREFIX = "10.23.45"

Vagrant.configure("2") do |config|

 config.vm.define "gateway" do |gateway|
   gateway.vm.box = "microlinux/rocky-8-base"
   gateway.vm.hostname = "gateway"
   gateway.vm.provider "libvirt" do |kvm|
     kvm.memory = 1024
     kvm.cpus = 2
   end
   gateway.vm.network :private_network, ip: "#{NETWORK_PREFIX}.10"
   gateway.vm.synced_folder ".", "/vagrant", type: "sshfs"
   gateway.vm.provision "shell",
     path: "setup-gateway.sh"
 end

 config.vm.define "server-01" do |server01|
   server01.vm.box = "microlinux/rocky-8-base"
   server01.vm.hostname = "server-01"
   server01.vm.provider "libvirt" do |kvm|
     kvm.memory = 1024
     kvm.cpus = 2
   end
   server01.vm.network :private_network, ip: "#{NETWORK_PREFIX}.20"
   server01.vm.synced_folder ".", "/vagrant", type: "sshfs"
   server01.vm.provision "shell",
     path: "setup-clients.sh"
 end

 config.vm.define "server-02" do |server02|
   server02.vm.box = "microlinux/rocky-8-base"
   server02.vm.hostname = "server-02"
   server02.vm.provider "libvirt" do |kvm|
     kvm.memory = 1024
     kvm.cpus = 2
   end
   server02.vm.network :private_network, ip: "#{NETWORK_PREFIX}.30"
   server02.vm.synced_folder ".", "/vagrant", type: "sshfs"
   server02.vm.provision "shell",
     path: "setup-clients.sh"
 end

 config.vm.define "client-01" do |client01|
   client01.vm.box = "microlinux/rocky-8-kde"
   client01.vm.hostname = "client-01"
   client01.vm.provider "libvirt" do |kvm|
     kvm.memory = 2048
     kvm.cpus = 2
   end
   client01.vm.network :private_network, ip: "#{NETWORK_PREFIX}.40"
   client01.vm.synced_folder ".", "/vagrant", type: "sshfs"
   client01.vm.provision "shell",
     path: "setup-clients.sh"
 end

 config.vm.define "client-02" do |client02|
   client02.vm.box = "microlinux/rocky-8-kde"
   client02.vm.hostname = "client-02"
   client02.vm.provider "libvirt" do |kvm|
     kvm.memory = 2048
     kvm.cpus = 2
   end
   client02.vm.network :private_network, ip: "#{NETWORK_PREFIX}.50"
   client02.vm.synced_folder ".", "/vagrant", type: "sshfs"
   client02.vm.provision "shell",
     path: "setup-clients.sh"
 end

end
