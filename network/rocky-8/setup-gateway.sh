#!/bin/bash
#
# setup-gateway.sh

# Firewall
echo "Configuring firewall."
firewall-cmd --permanent --zone=external --change-interface=eth0
firewall-cmd --permanent --zone=internal --change-interface=eth1
firewall-cmd --set-default-zone internal
firewall-cmd --permanent --add-masquerade
firewall-cmd --permanent --remove-service=cockpit
firewall-cmd --permanent --remove-service=dhcpv6-client
firewall-cmd --permanent --remove-service=mdns
firewall-cmd --permanent --remove-service=samba-client
firewall-cmd --permanent --add-service=ntp
firewall-cmd --permanent --add-service=dhcp
firewall-cmd --permanent --add-service=dns
firewall-cmd --reload

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# DNS
echo "Configuring DNS server."
dnf install -y dnsmasq
cp -v /vagrant/dnsmasq.conf /etc/
systemctl enable dnsmasq --now
cp -v /vagrant/resolv.conf /etc

exit 0
