#!/bin/bash
#
# setup.sh

# Logging
LOGS="/var/log/setup.log"
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Profile
PLASMA="/usr/share/plasma/plasmoids"
TASKBAR="${PLASMA}/org.kde.plasma.taskmanager/contents/config/main.xml"
KICKOFF="${PLASMA}/org.kde.plasma.kickoff/contents/config/main.xml"

# Post-install configuration script
echo "Installing Git."
dnf install -y git >> ${LOGS} 2>&1
echo "Fetching configuration script."
git clone https://gitlab.com/kikinovak/rocky-8-desktop >> ${LOGS} 2>&1
cd rocky-8-desktop
./setup.sh --shell
./setup.sh --repos
./setup.sh --tools
echo "  Installing X Window System."
dnf group install -y base-x >> ${LOGS} 2>&1
echo "  Installing basic fonts."
dnf install -y xorg-x11-fonts-* >> ${LOGS} 2>&1
dnf install -y dejavu-* >> ${LOGS} 2>&1
dnf install -y gnu-free* >> ${LOGS} 2>&1
echo "  Installing minimal KDE desktop environment."
dnf install -y plasma-desktop >> ${LOGS} 2>&1
echo "  Installing SDDM login manager."
dnf install -y sddm sddm-breeze >> ${LOGS} 2>&1
echo "  Installing basic applications."
dnf install -y dolphin konsole5 firefox >> ${LOGS} 2>&1
./setup.sh --menus
echo "  Configuring taskbar."
sed -i -e 's/applications:systemsettings.desktop/applications:org.kde.dolphin.desktop/g' ${TASKBAR}
sed -i -e 's/applications:org.kde.discover.desktop/applications:firefox.desktop/g' ${TASKBAR}
sed -i -e 's/preferred:\/\/filemanager//g' ${TASKBAR}
sed -i -e 's/preferred:\/\/browser//g' ${TASKBAR}
sleep 1
echo "  Configuring taskbar."
sed -i -e 's/suspend,hibernate/logout/g' ${KICKOFF}
echo "  Setting boot default to graphical environment."
sleep 1
systemctl set-default graphical.target >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless files."
dnf clean all >> ${LOGS} 2>&1

echo "  Rebooting..."
reboot

exit 0
