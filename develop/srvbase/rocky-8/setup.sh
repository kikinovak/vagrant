#!/bin/bash
#
# setup.sh

# Logging
LOGS="/var/log/setup.log"
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Post-install configuration script
echo "Installing Git."
dnf install -y git >> ${LOGS} 2>&1
echo "Fetching configuration script."
git clone https://gitlab.com/kikinovak/rocky-8-server >> ${LOGS} 2>&1
cd rocky-8-server
./setup.sh --shell
./setup.sh --repos
./setup.sh --tools
./setup.sh --ipv4
cd - >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless files."
dnf clean all >> ${LOGS} 2>&1

exit 0
