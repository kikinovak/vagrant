#!/bin/bash
#
# setup-host.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

exit 0
