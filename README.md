# Custom boxes for Vagrant Cloud + KVM

- `display/minimal/rocky-8`:

Minimal Rocky Linux 8 installation.

- `display/minimal/debian-10`:

Minimal Debian 10 installation.

- `display/minimal/debian-11`:

Minimal Debian 11 installation.

- `display/srvbase/rocky-8`:

Enhanced Rocky Linux 8 base system suitable for servers.

- `display/desktop/rocky-8`:

Minimal Rocky Linux 8 KDE desktop.

- `develop/srvbase/rocky-8`:

Development files for the `microlinux/rocky-8-base` box.

- `develop/desktop/rocky-8`:

Development files for the `microlinux/rocky-8-kde` box.

- `network/rocky-8`:

Virtual Rocky Linux 8 network consisting of one gateway, two servers and two
desktop clients.

- `cluster/rocky-8`:

Virtual Rocky Linux 8 cluster consisting of three headless servers.
