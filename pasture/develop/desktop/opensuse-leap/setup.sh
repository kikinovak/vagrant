#!/bin/bash
#
# setup.sh

# VirtualBox
VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.36"

# Logging
LOGS="/var/log/setup.log"
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Keyboard layout
echo "Configuring console keyboard layout: swiss french"
localectl set-keymap fr_CH-latin1
sleep 1

# Keep en_US.UTF-8 system locale
echo "Keeping en_US.UTF-8 system locale."
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd 
sleep 1

# Reinstall Guest Additions properly
KERNEL="$(rpm -q --queryformat '%{VERSION}-%{RELEASE}\n' kernel-default \
  | sort -nr | head -n 1)"
echo "Removing stale VirtualBox Guest Additions."
zypper remove -y virtualbox-kmp-default  >> ${LOGS} 2>&1
systemctl daemon-reload
zypper addlock virtualbox-kmp-default >> ${LOGS} 2>&1
zypper addlock virtualbox-guest-x11 >> ${LOGS} 2>&1
modprobe -r vboxguest
echo "Downloading VirtualBox ${BOXV} Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso >> ${LOGS} 2>&1
echo "Installing required build tools."
zypper install --no-recommends -y bzip2 gcc make \
  kernel-default-devel=${KERNEL} >> ${LOGS} 2>&1
echo "Installing VirtualBox ${BOXV} Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt >> ${LOGS} 2>&1
cd /mnt
./VBoxLinuxAdditions.run >> ${LOGS} 2>&1
cd - >> ${LOGS} 2>&1

# Remove previous kernel
# OLDKERNEL="$(rpm -q --queryformat '%{VERSION}-%{RELEASE}\n' kernel-default \
#   | sort -n | head -n 1)"
# echo "Removing old kernel packages."
# zypper remove -y kernel-default=${OLDKERNEL} >> ${LOGS} 2>&1
# echo "Locking current kernel version."
# zypper addlock kernel-{default,devel} >> ${LOGS} 2>&1
# zypper addlock kernel-default-{base,devel,extra,optional} >> ${LOGS} 2>&1

# Install basic package groups
PATTERNS="base \
          enhanced_base \
          yast2_basis \
          x11 \
          fonts \
          kde_plasma"
for PATTERN in ${PATTERNS}
do
  echo "Installing package group: ${PATTERN}"
  zypper install -y -t pattern ${PATTERN} >> ${LOGS} 2>&1
done

# Install extra packages
PACKAGES="git \
          konsole 
          net-tools-deprecated \
          yast2-trans-fr \
          man-pages-fr \
          MozillaFirefox-translations-common \
          discover"
echo "Installing extra packages."
zypper --non-interactive install --no-recommends ${PACKAGES} >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless packages and files."
rm -f VBoxGuestAdditions_${BOXV}.iso
zypper remove --clean-deps -y gcc make kernel-default-devel >> ${LOGS} 2>&1
zypper clean >> ${LOGS} 2>&1

# Root account
echo "Activating root account."
echo -e "vagrant\nvagrant\n" | passwd root >> ${LOGS} 2>&1

# Set VM resolution to 1024x768
echo "Configuring graphical resolution."
{ echo '# /etc/X11/xorg.conf.d/50-screen.conf'
  echo 'Section "Screen"'
  echo '  Identifier "Default Screen"'
  echo '  Device "Default Device"'
  echo '  Monitor "Default Monitor"'
  echo '  DefaultDepth 24'
  echo '  SubSection "Display"'
  echo '    Viewport 0 0'
  echo '    Depth    24'
  echo '    Modes    "1024x768"'
  echo '  EndSubSection'
  echo 'EndSection'
} > /etc/X11/xorg.conf.d/50-screen.conf

# GRUB
echo "Configuring boot manager."
sed -i -e 's/GRUB_GFXMODE=\"auto\"/GRUB_GFXMODE=\"1024x768\"/g' /etc/default/grub
sed -i -e 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"video=1024x768\"/g' /etc/default/grub
sed -i -e 's/GRUB_TIMEOUT=10/GRUB_TIMEOUT=3/g' /etc/default/grub
sed -i -e 's/splash=silent //g' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg >> ${LOGS} 2>&1

# Graphical session
echo "Activating graphical session."
systemctl set-default graphical.target >> ${LOGS} 2>&1

# Reboot
echo "Rebooting VM."
reboot

exit 0
