Build the OpenSUSE Leap 15.3 KDE VM
-----------------------------------

Hide VirtualBox menu.

KDE System Settings > Display and Monitor > Resolution: 1024x768 > Apply.

Configure Desktop > Wallpaper: Honeywave > OK.

YaST > System > Language > Primary Language: French - Français

  [ ] Adapt Keyboard Layout to French
  [x] Adapt Time Zone to Europe / France

KDE System Settings > Regional Settings > Add languages > français > ^ (top)

Configure Konsole.

Log out.

Reboot and check:

  $ vagrant reload

