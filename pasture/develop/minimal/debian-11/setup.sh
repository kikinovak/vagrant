#!/bin/bash
#
# setup.sh

# Variables
VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.36"
LOGS="/var/log/setup.log"

# Logging
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Keyboard layout
echo "Configuring console keyboard layout: fr-ch"
sed -i "s/XKBLAYOUT=\"us\"/XKBLAYOUT=\"ch\"/g" /etc/default/keyboard
sed -i "s/XKBVARIANT=\"\"/XKBVARIANT=\"fr\"/g" /etc/default/keyboard
# man 5 keyboard
setupcon

# Keep en_US.UTF-8 system locale
echo "Keeping en_US.UTF-8 system locale."
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd 
sleep 1

# Reinstall Guest Additions properly
echo "Downloading VirtualBox ${BOXV} Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso >> ${LOGS} 2>&1
echo "Installing VirtualBox ${BOXV} Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt >> ${LOGS} 2>&1
cd /mnt
./VBoxLinuxAdditions.run >> ${LOGS} 2>&1
cd - >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless files."
rm -f VBoxGuestAdditions_${BOXV}.iso
apt-get clean

# Reboot
echo "Rebooting VM."
reboot

exit 0
