#!/bin/bash
#
# build.sh

BOX="opensuse-leap-minimal"

vagrant halt

rm -f ${BOX}.box

vagrant package --output ${BOX}.box

sha256sum ${BOX}.box > ${BOX}.sha256sum

cat ${BOX}.sha256sum

exit 0
