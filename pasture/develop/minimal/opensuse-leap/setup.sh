#!/bin/bash
#
# setup.sh

# Variables
VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.36"
LOGS="/var/log/setup.log"

# Running kernel version
KVER="$(rpm -q --queryformat '%{VERSION}-%{RELEASE}\n' kernel-default \
  | sort -nr | head -n 1)"

# Logging
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Keyboard layout
echo "Configuring console keyboard layout: swiss french"
localectl set-keymap fr_CH-latin1
sleep 1

# Keep en_US.UTF-8 system locale
echo "Keeping en_US.UTF-8 system locale."
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd 
sleep 1

# Reinstall Guest Additions properly
echo "Removing stale VirtualBox Guest Additions."
zypper remove -y virtualbox-kmp-default  >> ${LOGS} 2>&1
systemctl daemon-reload
zypper addlock virtualbox-kmp-default >> ${LOGS} 2>&1
zypper addlock virtualbox-guest-x11 >> ${LOGS} 2>&1
modprobe -r vboxguest
echo "Downloading VirtualBox ${BOXV} Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso >> ${LOGS} 2>&1
echo "Installing required build tools."
zypper install --no-recommends -y bzip2 gcc make \
  kernel-default-devel=${KVER} >> ${LOGS} 2>&1
echo "Installing VirtualBox ${BOXV} Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt >> ${LOGS} 2>&1
cd /mnt
./VBoxLinuxAdditions.run >> ${LOGS} 2>&1
cd - >> ${LOGS} 2>&1

# Vim
echo "Installing Vim."
zypper install -y vim >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless packages and files."
rm -f VBoxGuestAdditions_${BOXV}.iso
zypper remove --clean-deps -y gcc make kernel-default-devel >> ${LOGS} 2>&1
zypper clean >> ${LOGS} 2>&1

# Reboot
echo "Rebooting VM."
reboot

exit 0
