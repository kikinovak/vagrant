#!/bin/bash
#
# setup.sh

# Variables
VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.36"
LOGS="/var/log/setup.log"

# Logging
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Keyboard layout
echo "Configuring console keyboard layout: swiss french"
localectl set-keymap fr_CH-latin1
sleep 1

# Reinstall Guest Additions properly
echo "Downloading VirtualBox ${BOXV} Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso >> ${LOGS} 2>&1
echo "Installing VirtualBox ${BOXV} Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt >> ${LOGS} 2>&1
cd /mnt
./VBoxLinuxAdditions.run >> ${LOGS} 2>&1
cd - >> ${LOGS} 2>&1

# Cleanup
echo "Removing useless files."
rm -f VBoxGuestAdditions_${BOXV}.iso

# Reboot
echo "Rebooting VM."
reboot

exit 0
