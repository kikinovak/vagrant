#!/bin/bash
#
# setup.sh

# Host
echo "Configuring host."
{ echo '# /etc/hosts'
  echo '127.0.0.1   localhost.localdomain localhost'
  echo '10.23.45.20 server-01.sandbox.lan server-01'
} > /etc/hosts

echo "Configuring DNS."
{ echo '# /etc/resolv.conf'
  echo 'search sandbox.lan'
  echo 'nameserver 10.23.45.10'
} > /etc/resolv.conf

echo "Configuring default gateway."
route del default
route add default gw 10.23.45.10

exit 0
