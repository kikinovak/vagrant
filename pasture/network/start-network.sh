#!/bin/bash
#
# start-network.sh

# Bring up gateway.sandbox.lan
cd gateway
vagrant up
# Bring up server-01.sandbox.lan
cd ../server-01
vagrant up
# Bring up server-02.sandbox.lan
cd ../server-02
vagrant up
# Bring up client-01.sandbox.lan
cd ../client-01
vagrant up
# Bring up client-02.sandbox.lan
cd ../client-02
vagrant up
# Bring up client-03.sandbox.lan
cd ../client-03
vagrant up
