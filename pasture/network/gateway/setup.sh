#!/bin/bash
#
# setup.sh

# Variables
LOGS="/var/log/setup.log"

# Logging
echo "Provisioning logs are written to ${LOGS}."
echo > ${LOGS}

# Firewall
echo "Configuring firewall."
firewall-cmd --permanent --zone=external --change-interface=enp0s3 >> ${LOGS} 2>&1
firewall-cmd --permanent --zone=internal --change-interface=enp0s8 >> ${LOGS} 2>&1
firewall-cmd --set-default-zone internal >> ${LOGS} 2>&1
firewall-cmd --permanent --add-masquerade >> ${LOGS} 2>&1
firewall-cmd --permanent --remove-service=cockpit >> ${LOGS} 2>&1
firewall-cmd --permanent --remove-service=dhcpv6-client >> ${LOGS} 2>&1
firewall-cmd --permanent --remove-service=mdns >> ${LOGS} 2>&1
firewall-cmd --permanent --remove-service=samba-client >> ${LOGS} 2>&1
firewall-cmd --permanent --add-service=ntp >> ${LOGS} 2>&1
firewall-cmd --permanent --add-service=dhcp >> ${LOGS} 2>&1
firewall-cmd --permanent --add-service=dns >> ${LOGS} 2>&1
firewall-cmd --reload >> ${LOGS} 2>&1

# NTP
echo "Configuring NTP server."
{ echo '# /etc/chrony.conf'
  echo 'server 0.fr.pool.ntp.org iburst'
  echo 'server 1.fr.pool.ntp.org iburst'
  echo 'server 2.fr.pool.ntp.org iburst'
  echo 'server 3.fr.pool.ntp.org iburst'
  echo 'driftfile /var/lib/chrony/drift'
  echo 'makestep 1.0 3'
  echo 'rtcsync'
  echo 'logdir /var/log/chrony'
} > /etc/chrony.conf
systemctl restart chronyd >> ${LOGS} 2>&1

# LAN
echo "Configuring local hosts."
{ echo '# /etc/hosts'
  echo '127.0.0.1   localhost.localdomain localhost'
  echo '10.23.45.10 gateway.sandbox.lan gateway'
  echo '10.23.45.20 server-01.sandbox.lan server-01'
  echo '10.23.45.30 server-02.sandbox.lan server-02'
  echo '10.23.45.40 client-01.sandbox.lan client-01'
  echo '10.23.45.50 client-02.sandbox.lan client-02'
  echo '10.23.45.60 client-03.sandbox.lan client-03'
} > /etc/hosts

# DNS
echo "Configuring DNS server."
dnf install -y dnsmasq >> ${LOGS} 2>&1
{ echo '# /etc/dnsmasq.conf'
  echo 'domain-needed'
  echo 'bogus-priv'
  echo 'interface=enp0s8'
  echo 'dhcp-range=10.23.45.100,10.23.45.200,24h'
  echo 'local=/sandbox.lan/'
  echo 'domain=sandbox.lan'
  echo 'expand-hosts'
  echo 'server=1.1.1.1'
  echo 'server=1.0.0.1'
  echo 'no-resolv'
  echo 'log-facility=/var/log/dnsmasq.log'
} > /etc/dnsmasq.conf
systemctl enable dnsmasq --now >> ${LOGS} 2>&1
{ echo '# /etc/resolv.conf'
  echo 'nameserver 127.0.0.1'
} > /etc/resolv.conf
