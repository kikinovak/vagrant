#!/bin/bash
#
# stop-network.sh

# Stop client-03.sandbox.lan
cd client-03
vagrant destroy -f
# Stop client-02.sandbox.lan
cd ../client-02
vagrant destroy -f
# Stop client-01.sandbox.lan
cd ../client-01
vagrant destroy -f
# Stop server-02.sandbox.lan
cd ../server-02
vagrant destroy -f
# Stop server-01.sandbox.lan
cd ../server-01
vagrant destroy -f
# Stop gateway.sandbox.lan
cd ../gateway
vagrant destroy -f
