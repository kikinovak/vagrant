#!/bin/bash
#
# setup-client.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# NFS
dnf install -y nfs-utils
mv -v /etc/idmapd.conf /etc/idmapd.conf.orig
cp -v /vagrant/idmapd.conf /etc/
killall -u vagrant && userdel -r vagrant
mount -t nfs server:/home /home
echo "server:/home  /home  nfs  defaults  0 0" >> /etc/fstab

# NIS
dnf install -y ypbind
ypdomainname sandbox.lan
cp -v /vagrant/network.client /etc/sysconfig/network
echo "domain sandbox.lan server server.sandbox.lan" >> /etc/yp.conf
authselect select nis --force
setsebool -P nis_enabled on
systemctl enable rpcbind ypbind nis-domainname --now

exit 0
