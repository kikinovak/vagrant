#!/bin/bash
#
# setup-server.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# Firewall
firewall-cmd --permanent --remove-service=cockpit
firewall-cmd --permanent --remove-service=dhcpv6-client
firewall-cmd --permanent --add-service=nfs
firewall-cmd --permanent --add-service=rpc-bind
firewall-cmd --permanent --add-port={944-951/tcp,944-951/udp}
firewall-cmd --reload

# NFS
dnf install -y nfs-utils
mv -v /etc/idmapd.conf /etc/idmapd.conf.orig
cp -v /vagrant/idmapd.conf /etc/
mkdir -v /srv/data
cp -v /vagrant/exports /etc/
systemctl enable rpcbind nfs-server --now

# NIS
dnf install -y ypserv
ypdomainname sandbox.lan
cp -v /vagrant/network.server /etc/sysconfig/network
cp -v /vagrant/yppasswdd /etc/sysconfig/
cp -v /vagrant/securenets /var/yp/
systemctl enable ypserv ypxfrd yppasswdd nis-domainname --now
/usr/lib64/yp/ypinit -m
setsebool -P nis_enabled on
setsebool -P domain_can_mmap_files on

exit 0
