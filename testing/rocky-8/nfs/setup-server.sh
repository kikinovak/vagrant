#!/bin/bash
#
# setup-host.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# Firewall
firewall-cmd --permanent --add-service=nfs
firewall-cmd --reload

# NFS
dnf install -y nfs-utils
mv -v /etc/idmapd.conf /etc/idmapd.conf.orig
cp -v /vagrant/idmapd.conf /etc/
mkdir -v /srv/{data,home}
cp -v /vagrant/exports /etc/
echo "This is a test." > /srv/data/nfs-export-test.txt
systemctl enable rpcbind nfs-server --now

exit 0
