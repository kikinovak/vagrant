#!/bin/bash
#
# setup-host.sh

# LAN
echo "Configuring local hosts."
cp -v /vagrant/hosts /etc/

# NTP
echo "Configuring NTP server."
cp -v /vagrant/chrony.conf /etc/
systemctl restart chronyd

# NFS
dnf install -y nfs-utils
mv -v /etc/idmapd.conf /etc/idmapd.conf.orig
cp -v /vagrant/idmapd.conf /etc/
mkdir -pv /mnt/nfs/{data,home}
mount -t nfs server:/srv/data /mnt/nfs/data/
mount -t nfs server:/srv/home /mnt/nfs/home/
echo "server:/srv/data  /mnt/nfs/data  nfs  defaults  0 0" >> /etc/fstab
echo "server:/srv/home  /mnt/nfs/home  nfs  defaults  0 0" >> /etc/fstab

exit 0
